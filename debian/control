Source: fonts-gfs-didot-classic
Section: fonts
Priority: optional
Maintainer: Debian Fonts Task Force <pkg-fonts-devel@lists.alioth.debian.org>
Uploaders: Nicolas Spalinger <nicolas.spalinger@sil.org>,
           Faidon Liambotis <paravoid@debian.org>,
           Hideki Yamane <henrich@debian.org>,
Build-Depends: debhelper-compat (= 13),
Standards-Version: 4.5.0
Homepage: https://www.greekfontsociety.gr/
Vcs-Git: https://salsa.debian.org/fonts-team/fonts-gfs-didot-classic.git
Vcs-Browser: https://salsa.debian.org/fonts-team/fonts-gfs-didot-classic
Rules-Requires-Root: no

Package: fonts-gfs-didot-classic
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: greek font family (Classic Didot revival)
 Under the influence of the neoclassical ideals of the late 18th century, the
 famous French typecutter Firmin Didot in Paris designed a new Greek typeface
 (1805) which was immediately used in the publishing programme of Adamantios
 Korai, the prominent intellectual figure of the Greek diaspora and leading
 scholar of the Greek Enlightenment. The typeface eventually arrived in Greece,
 with the field press which came with Didot’s grandson Ambroise Firmin Didot,
 during the Greek Revolution in 1821.
 .
 Since then the typeface has enjoyed an unrivaled success as the type of choice
 for almost every kind of publication until the last decades of the 20th
 century. Didot's original type design, as it is documented in publications
 during the first decades of the 19th century, was digitized and revived by
 George D. Matthiopoulos in 2006 for a project of the Department of Literature
 in the School of Philosophy at the University of Thessaloniki, and is now
 available for general use.
